import java.util.*;

/******************************************************************
 * Purpose: Create Plan objects and load them into an array list
 * Import: 2D array of plan strings
 * Export: Plan List
 * Author: Antony LeGall
 *****************************************************************/

public class InitializePlan
{
	public PlanList initializePlan(String[][] planArray)
	{
		String[] planString;
		Plan plan;
		Factory factory = new Factory();
		PlanList planList = new PlanList();
		//Go through the plan list creating objects
		for(int i = 0; i < planArray.length; i++)
		{
			planString = planArray[i];
			//Delegate the creation of the objects to the factory
			plan = factory.makePlan(planString);
			planList.add(plan);
			planList.updateSize();
		}
		return planList;
	}
}