import java.util.*;

/******************************************************************
 * Purpose:  Event subclass to store wage event information and execute events
 * Import: 
 * Export: 
 * Author: Antony LeGall
 ****************************************************************/

public class WageEvent implements Event
{
	private int year;
	private String type;

	public WageEvent(int inYear, String inType)
	{
		setYear(inYear);
		setType(inType);
	}

	//Mutators
	public void setYear(int inYear)
	{
		year = inYear;
	}

	public void setType(String inType)
	{
		if(inType.equals(""))
		{
			throw new IllegalArgumentException("The Type of event cannot be blank");
		}
		else
		{
			type = inType;
		}
	}

	//Getters
	public int getYear()
	{
		return year;
	}

	public String getType()
	{
		return type;
	}

	public	PropertyList executeEvent(PropertyList propertyList)
	{
		Property property;
		double wageChange, newWage;
		if(type.equals("W+"))
		{
			wageChange = 1.05;
		}
		else
		{
			wageChange = 0.95;
		}
		//Go through the list getting every business and modifying wages
		for(int i = 0; i < propertyList.getSize(); i++)
		{
			property = propertyList.remove(i);
			if(property instanceof Business)
			{
				newWage = property.getWages();
				newWage = newWage * wageChange;
				property.setWages(newWage);
			}
			propertyList.add(i, property);
		}
		return propertyList;
	}
}