import java.util.*;

/******************************************************************
 * Purpose:  Factory class to handle creating objects
 * Import: 
 * Export:  Created Object
 * Author: Antony LeGall
 ****************************************************************/

public class Factory
{
	//Create and return property objects depending on type
	public Property makeProperty(String[] property, boolean isPrimary)
	{
		Property propertyObj = null;
		String type = property[1];
		if(type.equals("C"))
		{
			if(isPrimary == true)
			{
				propertyObj = new Company(property[0], property[2], Double.parseDouble(property[3]), true);	
			}
			else
			{
				propertyObj = new Company(property[0], property[2], Double.parseDouble(property[3]), false);
			}
		}
		else
		{
			propertyObj = new Business(property[0], property[2], Double.parseDouble(property[3]),
				Double.parseDouble(property[4]), Double.parseDouble(property[5]));
		}
		return propertyObj;
	}

	//Create and return event objects depending on type
	public Event makeEvent(String[] event)
	{
		Event eventObj = null;
		String type = event[1];
		if(type.charAt(0) == 'W')
		{
			eventObj = new WageEvent(Integer.parseInt(event[0]), event[1]);
		}
		else if(type.charAt(0) == 'R')
		{
			eventObj = new RevenueEvent(Integer.parseInt(event[0]), event[1], event[2]);
		}
		else
		{
			eventObj = new ValueEvent(Integer.parseInt(event[0]), event[1], event[2]);
		}
		return eventObj;
	}

	//Create and return plan objects depending on type
	public Plan makePlan(String[] plan)
	{
		Plan planObj = null;
		String type = plan[1];
		if(type.charAt(0) == 'B')
		{
			planObj = new Buy(Integer.parseInt(plan[0]), plan[1], plan[2]);
		}
		else
		{
			planObj = new Sell(Integer.parseInt(plan[0]), plan[1], plan[2]);
		}
		return planObj;
	}
}