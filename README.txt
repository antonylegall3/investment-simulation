README for an application to simulate a companys profit/loss over a specified time period. Buy and sell moves are specified in a text file which also outlines the other companys that can be traded.
The end result is a thorough report of the financial outcomes of these decisions. The program was written to utilize as many software design patterns as applicable. The following patterns were used:
Strategy Pattern, Dependency Injection Pattern, Factory Pattern.

Repository File Structure:
	- All src files present in main directory

Compilation:
	- Navigate to the main directory
	
		$ javac *.java

Running:
	- Navigate to the main directory
	
		$ java Main [propertyfile] [eventfile] [planfile] startYear endYear
		
	- Replacing each file with your own test files.