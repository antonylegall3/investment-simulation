import java.util.*;

/******************************************************************
 * Purpose: Create Property objects and load them into an array list
 * Import: 2D array of property strings
 * Export: Property List
 * Author: Antony LeGall
 *****************************************************************/

public class InitializeProperty
{
	public PropertyList initializeProperty(String[][] propertyArray)
	{
		String[] propertyString;
		boolean isPrimary = false, primaryFound = false;
		Property property, secondaryProperty;
		Property[] ownedProperty;
		Factory factory = new Factory();
		PropertyList propertyList = new PropertyList();
		PropertyList finalList = new PropertyList();

		//Iterate through the 2D array to create property objects
		for(int i = 0; i < propertyArray.length; i++)
		{
			propertyString = propertyArray[i];
			//Check the line to see if its the primary company
			if(propertyArray[i][1].equals("C") && (primaryFound == false))
			{
				//Set the primary flag to true to create the primary company
				isPrimary = true;
				//Set the primary found flag to true so we only create 1 primary company
				primaryFound = true;
				//Delegate the creation of the objects to the factory
				property = factory.makeProperty(propertyString, isPrimary);
				isPrimary = false;
			}
			else
			{
				//Otherwise its a business or non-primary company
				property = factory.makeProperty(propertyString, isPrimary);
			}
			//Add it to the propertyList
			propertyList.add(property);
			propertyList.updateSize();
		}

		//Initialize all companys owned property arrays
		for(int j = 0; j < propertyList.getSize(); j++)
		{
			property = propertyList.get(j);
			ownedProperty = property.getOwnedProperty();
			//If its a company check if any of the other property belong to it
			if(property instanceof Company)
			{
				for(int k = 0; k < propertyList.getSize(); k++)
				{
					secondaryProperty = propertyList.get(k);
					if(secondaryProperty.getOwner().equals(property.getName()))
					{
						//If they do belong to the initial company store them
						ownedProperty[property.getSize()] = secondaryProperty;
						property.updateSize();
					}
				}
				property.setOwnedProperty(ownedProperty);
			}
			finalList.add(property);
			finalList.updateSize();
		}
		return finalList;
	}
}