import java.util.*;
import java.io.*;

/******************************************************************
 * Purpose: Read CSV files and put them in a 2D array
 * Import: FileName
 * Export: 2D Array
 * Author: Antony LeGall
 ****************************************************************/

public class ReadFile
{
	public String[][] readFile(String fileName)
	{		
		String[][] fileContents = null;
		try
		{
			//Open a reader on the given file
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			String line = reader.readLine();
			int numLines = 0, i = 0;
			double num;
			String[] parts;
			//Run through the file once to get the number of lines and verify the file format is correct
			if(line.equals("Name,Type,Owner,Worth,Revenue,Wages"))
			{
				line = reader.readLine();
				while(line != null)
				{
					numLines++;
					parts = line.split(",");
					for(int j = 3; j < parts.length; j++)
					{
						num = Double.parseDouble(parts[j]);
						if(num < 0.0)
						{
							throw new IOException("Incorrect File format: Property file cannot have negative numbers");
						}
					}

					line = reader.readLine();
				}	
			}
			else if(line.equals("Year,Event,Property"))
			{
				line = reader.readLine();
				while(line != null)
				{
					numLines++;
					parts = line.split(",");
					if(Integer.parseInt(parts[0]) < 0)
					{
						throw new IOException("Incorrect File format: Cannot have negative year in event file");
					}
					line = reader.readLine();
				}
			}
			else if(line.equals("Year,Buy/Sell,Property"))
			{
				line = reader.readLine();
				while(line != null)
				{
					numLines++;
					parts = line.split(",");
					if(Integer.parseInt(parts[0]) < 0)
					{
						throw new IOException("Incorrect File format: Cannot have negative year in plan file");
					}
					line = reader.readLine();
				}
			}
			else
			{
				throw new IOException("Incorrect File Format: Unrecognised file type");
			}

			fileContents = new String[numLines][6];

			//Open another reader to go through the file again
			BufferedReader reader2 = new BufferedReader(new FileReader(fileName));

			line = reader2.readLine();
			line = reader2.readLine();
			//Run through the file storing each line in the 2D array
			while(line != null)
			{
				parts = line.split(",");
				fileContents[i] = parts;
				i++;
				line = reader2.readLine();
			}
		}
		catch(IOException e)
		{
			System.out.println("Error Reading File: " + e);
		}
		return fileContents;
	}
}