import java.util.*;

public class Sell implements Plan
{
	private int year;
	private String type;
	private String property;

	public Sell(int inYear, String inType, String inProperty)
	{
		setYear(inYear);
		setType(inType);
		setProperty(inProperty);
	}

	//Mutators
	public void setYear(int inYear)
	{
		year = inYear;
	}

	public void setType(String inType)
	{
		if(inType.equals(""))
		{
			throw new IllegalArgumentException("The Type of event cannot be blank");
		}
		else
		{
			type = inType;
		}
	}

	public void setProperty(String inProperty)
	{
		if(inProperty.equals(""))
		{
			throw new IllegalArgumentException("Property cannot be blank");
		}
		else
		{
			property = inProperty;
		}
	}

	//Getters
	public int getYear()
	{
		return year;
	}

	public String getType()
	{
		return type;
	}

	public String getProperty()
	{
		return property;
	}

	public void executePlan(PropertyList propertyList)
	{
		int index, z;
		double newBalance, sellerBalance;
		BankAccount bankAccount, sellerBank;
		Property primaryCompany, soldProperty, sellerCompany;
		PropertyList companyPropList;
		Property[] ownedProp;
		for(int i = 0; i < propertyList.getSize(); i++)
		{
			primaryCompany = propertyList.remove(i);
			//Find the primary company
			if(primaryCompany.getIsPrimary() == true)
			{
				//Get its bank account
				bankAccount = primaryCompany.getBankAccount();
				ownedProp = primaryCompany.getOwnedProperty();
				//Find the sold company in the primary companys property array
				for(int j = 0; j < primaryCompany.getSize(); j++)
				{
					soldProperty = ownedProp[j];
					//Check to see if this is the company being sold
					if(soldProperty.getName().equals(getProperty()))
					{
						//Add the worth of the sold property to the primary bank account
						newBalance = soldProperty.getWorth();
						bankAccount.setBalance(newBalance);
						primaryCompany.setBankAccount(bankAccount);
						//Set the owner of this property to blank in the plist
						index = propertyList.indexOf(soldProperty);
						soldProperty = propertyList.remove(index);
						soldProperty.setOwner("");
						propertyList.add(index, soldProperty);
						//Remove this property from the owned property array
						ownedProp[j] = null;

						//Shuffle all items to the right of the sold property down 1
						for(int k = j; k < primaryCompany.getSize() - j; k++)
						{
							z = k + 1;
							ownedProp[k] = ownedProp[z];
						}
						primaryCompany.decrementSize();
						primaryCompany.setOwnedProperty(ownedProp);
					}
				}
			}
			propertyList.add(i, primaryCompany);
		}
	}
}