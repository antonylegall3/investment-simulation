import java.util.*;

/******************************************************************
 * Purpose:  Event subclass to store revenue event information and execute events
 * Import: 
 * Export: 
 * Author: Antony LeGall
 ****************************************************************/

public class RevenueEvent implements Event
{
	private int year;
	private String type;
	private String property;

	public RevenueEvent(int inYear, String inType, String inProperty)
	{
		setYear(inYear);
		setType(inType);
		setProperty(inProperty);
	}

	//Mutators
	public void setYear(int inYear)
	{
		year = inYear;
	}

	public void setType(String inType)
	{
		if(inType.equals(""))
		{
			throw new IllegalArgumentException("The Type of event cannot be blank");
		}
		else
		{
			type = inType;
		}
	}

	public void setProperty(String inProperty)
	{
		if(inProperty.equals(""))
		{
			throw new IllegalArgumentException("Property cannot be blank");
		}
		else
		{
			property = inProperty;
		}
	}

	//Getters
	public int getYear()
	{
		return year;
	}

	public String getType()
	{
		return type;
	}

	public String getProperty()
	{
		return property;
	}

	public 	PropertyList executeEvent(PropertyList propertyList)
	{
		double newRevenue, revenueChange;
		Property property;
		if(type.equals("R+"))
		{
			revenueChange = 1.05;
		}
		else
		{
			revenueChange = 0.95;
		}
		//Go through the list until you find the specified property and modify its revenue
		for(int i = 0; i < propertyList.getSize(); i++)
		{
			property = propertyList.remove(i);
			if(property.getName().equals(getProperty()))
			{
				newRevenue = property.getRevenue();
				newRevenue = newRevenue * revenueChange;
				property.setRevenue(newRevenue);
			}
			propertyList.add(i, property);
		}
		return propertyList;
	}
}