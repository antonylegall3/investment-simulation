import java.util.*;

/******************************************************************
 * Purpose:  Simple array list to store plan objects
 * Import: 
 * Export: 
 * Author: Antony LeGall
 ****************************************************************/

public class PlanList
{
    private ArrayList<Plan> planList;
    private int size = 0;

    public PlanList()
    {
        planList = new ArrayList<Plan>();
    }

    public void add(Plan plan)
    {
        planList.add(plan);
    }

    public void updateSize()
    {
        size++;
    }

    public int getSize()
    {
        return size;
    }

    public Plan get(int index)
    {
        return planList.get(index);
    }
}