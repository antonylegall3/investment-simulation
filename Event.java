import java.util.*;

/******************************************************************
 * Purpose:  Provide an interface for events to override these functions
 * Import: 
 * Export: 
 * Author: Antony LeGall
 ****************************************************************/

public interface Event
{
	PropertyList executeEvent(PropertyList propertyList);
	int getYear();
	String getType();
}