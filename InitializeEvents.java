import java.util.*;

/******************************************************************
 * Purpose: Create Event objects and load them into an array list
 * Import: 2D array of event strings
 * Export: Event List
 * Author: Antony LeGall
 *****************************************************************/

public class InitializeEvents
{
	public EventList initializeEvents(String[][] eventArray)
	{
		String[] eventString;
		Event event;
		Factory factory = new Factory();
		EventList eventList = new EventList();
		//Go through the event list creating objects
		for(int i = 0; i < eventArray.length; i++)
		{
			eventString = eventArray[i];
			//Delegate the creation of the objects to the factory
			event = factory.makeEvent(eventString);
			eventList.add(event);
			eventList.updateSize();
		}
		return eventList;
	}
}