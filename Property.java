import java.util.*;

/******************************************************************
 * Purpose:  Provide an interface for property objects to override these functions
 * Import: 
 * Export: 
 * Author: Antony LeGall
 ****************************************************************/

public interface Property
{
	//Useful Interface functions
	void profitCalculation();
	String getName();
	void setName(String inName);
	double getWorth();
	void setWorth(double inWorth);
	String getOwner();
	double getProfit();
	void setOwner(String inOwner);
	void setProfit(double inProfit);
	//Not Useful functions
	double getWages();
	void setWages(double inWages);
	double getRevenue();
	void setRevenue(double inRevenue);
	boolean getIsPrimary();
	BankAccount getBankAccount();
	Property[] getOwnedProperty();
	int getSize();
	void updateSize();
	void decrementSize();
	void setOwnedProperty(Property[] inOwnedProperty);
	void setBankAccount(BankAccount inBankAccount);
	void removeCompany(String inName);
}