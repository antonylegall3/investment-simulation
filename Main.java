import java.util.*;

/******************************************************************
 * Purpose:  Main class to tie the system together and provide dependency injection
 * Import:  Command line arguments
 * Export: 
 * Author: Antony LeGall
 ****************************************************************/

public class Main
{
	public static void main(String [] args)
	{
		String file = args[0];
		String file2 = args[1];
		String file3 = args[2];
		int startYear = Integer.parseInt(args[3]), endYear = Integer.parseInt(args[4]);
		int currentYear = startYear;

		String[][] propertyArray, eventArray, planArray;

		ReadFile readFile = new ReadFile();

		InitializeProperty iP = new InitializeProperty();
		InitializeEvents iE = new InitializeEvents();
		InitializePlan iPlan = new InitializePlan();

		PropertyList propertyList = new PropertyList();
		EventList eventList = new EventList();
		PlanList planList = new PlanList();

		Property property;
		BankAccount bankAccount;

		//Read each CSV file into 2D arrays
		propertyArray = readFile.readFile(file);
		eventArray = readFile.readFile(file2);
		planArray = readFile.readFile(file3);

		//Create the Property,Event and Plan objects and load them into lists
		propertyList = iP.initializeProperty(propertyArray);
		eventList = iE.initializeEvents(eventArray);
		planList = iPlan.initializePlan(planArray);

		//Run the simulation until the end year
		while(currentYear <= endYear)
		{
			//This is not meant to occur the first year
			if(currentYear != startYear)
			{
				//Update the bank accounts for each company to reflect profit/loss
				for(int z = 0; z < propertyList.getSize(); z++)
				{
					property = propertyList.remove(z);
					//If its a company without an owner calculate its profit from property it owns and update its bank details
					if((property instanceof Company) && (property.getOwner().equals("")))
					{
						property.profitCalculation();
					}
					propertyList.add(z, property);
				}
			}

			//Output current year,company name and balance in a nice format
			for(int x = 0; x < propertyList.getSize(); x++)
			{
				property = propertyList.get(x);
				bankAccount = property.getBankAccount();
				if(property instanceof Company)
				{
					System.out.print(currentYear + ": " + property.getName() + ": ");
					System.out.printf("%.2f",bankAccount.getBalance());
					System.out.println();
				}
				propertyList.add(property);
			}
			System.out.println("==================================");

			
			//Handle the events for each year
			for(int i = 0; i < eventList.getSize(); i++)
			{
				Event event = eventList.get(i);
				//If the event occurs this year execute it
				if(event.getYear() == currentYear)
				{
					propertyList = event.executeEvent(propertyList);
				}
			}

			//Handle the plan for the year
			for(int j = 0; j < planList.getSize(); j++)
			{
				Plan plan = planList.get(j);
				//If the plan decision occurs this year execute it
				if(plan.getYear() == currentYear)
				{
					try
					{
						plan.executePlan(propertyList);
					}
					catch(IllegalArgumentException e)
					{
						System.out.println("Error occured executing a plan: " + e);
					}
				}
			}

			//At the end of the year increment the year
			currentYear++;
		}
	}
}