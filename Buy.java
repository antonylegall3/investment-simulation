import java.util.*;

/******************************************************************
 * Purpose:  Plan subclass to store plan information and execute buying decisions
 * Import: 
 * Export: 
 * Author: Antony LeGall
 ****************************************************************/

public class Buy implements Plan
{
	private int year;
	private String type;
	private String property;

	public Buy(int inYear, String inType, String inProperty)
	{
		setYear(inYear);
		setType(inType);
		setProperty(inProperty);
	}

	//Mutators
	public void setYear(int inYear)
	{
		year = inYear;
	}

	public void setType(String inType)
	{
		if(inType.equals(""))
		{
			throw new IllegalArgumentException("The Type of event cannot be blank");
		}
		else
		{
			type = inType;
		}
	}

	public void setProperty(String inProperty)
	{
		if(inProperty.equals(""))
		{
			throw new IllegalArgumentException("Property cannot be blank");
		}
		else
		{
			property = inProperty;
		}
	}

	//Getters
	public int getYear()
	{
		return year;
	}

	public String getType()
	{
		return type;
	}

	public String getProperty()
	{
		return property;
	}

	public void executePlan(PropertyList propertyList) throws IllegalArgumentException
	{
		double newBalance, ownerBalance;
		BankAccount bankAccount, ownerBank;
		Property primaryCompany, boughtProperty, ownerCompany;
		PropertyList companyPropList;
		Property[] ownedProp;
		//Iterate through the list to find the primary company
		for(int i = 0; i < propertyList.getSize(); i++)
		{
			primaryCompany = propertyList.remove(i);
			//Check if its primary
			if(primaryCompany.getIsPrimary() == true)
			{
				//Get its bank account
				bankAccount = primaryCompany.getBankAccount();
				//Iterate through the list to find the bought company
				for(int j = 0; j< propertyList.getSize(); j++)
				{
					boughtProperty = propertyList.remove(j);
					//Check if its the bought company
					if(boughtProperty.getName().equals(getProperty()))
					{
						//If this is the bought company check if its buying itself
						if(primaryCompany.getName().equals(boughtProperty.getName()))
						{
							throw new IllegalArgumentException("Company: " + primaryCompany.getName() + " Cannot buy itself");
						}
						else
						{
							//Update the primary companys bank account
							newBalance = boughtProperty.getWorth() * (-1);
							bankAccount.setBalance(newBalance);
							primaryCompany.setBankAccount(bankAccount);
							
							//Find the company who sold this property and update its bank account
							if(!boughtProperty.getName().equals(""))
							{
								for(int k = 0; k < propertyList.getSize(); k++)
								{
									ownerCompany = propertyList.remove(k);
									if(ownerCompany.getName().equals(boughtProperty.getOwner()))
									{
										//Update its bank balance
										ownerBank = ownerCompany.getBankAccount();
										ownerBalance = ownerBank.getBalance();
										ownerBalance = boughtProperty.getWorth();
										ownerBank.setBalance(ownerBalance);
										ownerCompany.setBankAccount(ownerBank);
										//Remove the company it sold from its property array
										ownerCompany.removeCompany(boughtProperty.getName());
										ownerCompany.decrementSize();
									}
									propertyList.add(k, ownerCompany);
								}
							}
							//Update the bought property owner name
							boughtProperty.setOwner(primaryCompany.getName());
							//Add it to owned company array
							ownedProp = primaryCompany.getOwnedProperty();
							ownedProp[primaryCompany.getSize()] = boughtProperty;
							primaryCompany.updateSize();
						}
					}
					//Put the property back in the list regardless of if it was bought
					propertyList.add(j, boughtProperty);
				}	
			}
			propertyList.add(i, primaryCompany);
		}
	}
}