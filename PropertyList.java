import java.util.*;

/******************************************************************
 * Purpose:  Simple array list to store property objects
 * Import: 
 * Export: 
 * Author: Antony LeGall
 ****************************************************************/

public class PropertyList
{
    private ArrayList<Property> propertyList;
    private int size = 0;

    public PropertyList()
    {
        propertyList = new ArrayList<Property>();
    }

    public void add(Property property)
    {
        propertyList.add(property);
    }

    public void add(int index, Property property)
    {
        propertyList.add(index, property);
    }

    public int indexOf(Property property)
    {
        return propertyList.indexOf(property);
    }

    public void updateSize()
    {
        size++;
    }

    public void decrementSize()
    {
        size--;
    }

    public int getSize()
    {
        return size;
    }

    public Property get(int index)
    {
        return propertyList.get(index);
    }

    public Property remove(int index)
    {
        return propertyList.remove(index);
    }
}