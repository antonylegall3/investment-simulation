import java.util.*;

/******************************************************************
 * Purpose:  Container class to store company information and calculate company profit
 * Import: 
 * Export: 
 * Author: Antony LeGall
 ****************************************************************/

public class Company implements Property
{
	private String name;
	private String owner;
	private double worth;
	private double profit;
	private boolean isPrimary;
	private BankAccount bankAccount;
	private Property[] ownedProperty;
	private int arraySize;
	
	public Company(String inName, String inOwner, double inWorth, boolean isPrimary)
	{
		setName(inName);
		setPrimary(isPrimary);
		setOwner(inOwner);
		setWorth(inWorth);
		bankAccount = new BankAccount(inName);
		ownedProperty = new Property[100];
		arraySize = 0;
	}

	//Mutators
	public void setName(String inName)
	{
		if(inName.equals(""))
		{
			throw new IllegalArgumentException("Name cannot be blank");
		}
		else
		{
			name = inName;
		}
	}

	public void setOwner(String inOwner)
	{
		owner = inOwner;
	}

	public void setWorth(double inWorth)
	{
		worth = inWorth;
	}

	public void setPrimary(boolean primary)
	{
		isPrimary = primary;
	}

	public void setProfit(double inProfit)
	{
		profit = inProfit;
	}

	public void setBankAccount(BankAccount inBankAccount)
	{
		bankAccount = inBankAccount;
	}

	//Getters
	public String getName()
	{
		return name;
	}

	public String getOwner()
	{
		return owner;
	}

	public double getWorth()
	{
		return worth;
	}

	public double getProfit()
	{
		return profit;
	}

	public boolean getIsPrimary()
	{
		return isPrimary;
	}

	public BankAccount getBankAccount()
	{
		return bankAccount;
	}

	public Property[] getOwnedProperty()
	{
		return ownedProperty;
	}

	public void updateSize()
	{
		arraySize++;
	}

	public void decrementSize()
	{
		arraySize--;
	}

	public void setOwnedProperty(Property[] inOwnedProperty)
	{
		ownedProperty = inOwnedProperty;
	}

	public int getSize()
	{
		return arraySize;
	}

	//Interface Functions
	public double getWages()
	{
		throw new UnsupportedOperationException();
	}

	public void setWages(double inWages)
	{
		throw new UnsupportedOperationException();
	}
	public double getRevenue()
	{
		throw new UnsupportedOperationException();
	}

	public void setRevenue(double inRevenue)
	{
		throw new UnsupportedOperationException();
	}

	//Function to remove a company from the owned property array and shuffle it down
	public void removeCompany(String inName)
	{
		Property prop;
		int z;
		for(int i = 0; i < arraySize; i++)
		{
			prop = ownedProperty[i];
			//Check if its the company and then remove it
			if(prop.getName().equals(inName))
			{
				ownedProperty[i] = null;
				//Shuffle it down to fill the open space
				for(int k = i; k < arraySize - i; k++)
				{
					z = k + 1;
					ownedProperty[k] = ownedProperty[z];
				}
			}
		}

	}

	public void profitCalculation()
	{
		profit = 0.0;
		Property property = null;
		profit = bankAccount.profitCalculation();

		//Calculate profit for its owned companys
		for(int i = 0; i < getSize(); i++)
		{
			property = ownedProperty[i];
			if(property != null)
			{
				property.profitCalculation();
				profit += property.getProfit();
			}
		}
		//If profit is negative we want to subtract the entire amount off the bank
		if(profit < 0.0)
		{
			bankAccount.setBalance(profit);
			profit = 0.0;
		}
		//If its positive we take half to the bank and give half to the owner
		else
		{
			profit = profit / 2.0;
			bankAccount.setBalance(profit);
		}
	}
}