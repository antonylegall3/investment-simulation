import java.util.*;

/******************************************************************
 * Purpose:  Provide an interface for plan objects to override these functions
 * Import: 
 * Export: 
 * Author: Antony LeGall
 ****************************************************************/

public interface Plan
{
	void executePlan(PropertyList propertyList);
	int getYear();
	String getType();
	String getProperty();
}