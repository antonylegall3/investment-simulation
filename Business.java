import java.util.*;

/******************************************************************
 * Purpose:  Container class to store business information and calculate business profit
 * Import: 
 * Export: 
 * Author: Antony LeGall
 ****************************************************************/

public class Business implements Property
{
	private String name;
	private String owner;
	private double worth;
	private double revenue;
	private double wages;
	private double profit;
	
	public Business(String inName, String inOwner, double inWorth, double inRevenue,
		double inWages)
	{
		setName(inName);
		setOwner(inOwner);
		setWorth(inWorth);
		setRevenue(inRevenue);
		setWages(inWages);
	}

	//Mutators
	public void setName(String inName)
	{
		if(inName.equals(""))
		{
			throw new IllegalArgumentException("Name cannot be blank");
		}
		else
		{
			name = inName;
		}
	}

	public void setOwner(String inOwner)
	{
		owner = inOwner;
	}

	public void setWorth(double inWorth)
	{
		worth = inWorth;
	}

	public void setRevenue(double inRevenue)
	{
		revenue = inRevenue;
	}

	public void setWages(double inWages)
	{
		wages = inWages;
	}

	public void setProfit(double inProfit)
	{
		profit = inProfit;
	}

	//Getters
	public String getName()
	{
		return name;
	}

	public String getOwner()
	{
		return owner;
	}

	public double getWorth()
	{
		return worth;
	}

	public double getRevenue()
	{
		return revenue;
	}

	public double getWages()
	{
		return wages;
	}

	public double getProfit()
	{
		return profit;
	}

	//Interface functions
	public Property[] getOwnedProperty()
	{
		Property[] p = null;
		return p;
	}

	public void setOwnedProperty(Property[] inOwnedProperty)
	{
		throw new UnsupportedOperationException();
	}
	
	public void setBankAccount(BankAccount inBankAccount)
	{
		throw new UnsupportedOperationException();
	}

	public void updateSize()
	{
		throw new UnsupportedOperationException();
	}

	public void removeCompany(String inName)
	{
		throw new UnsupportedOperationException();
	}
	public void decrementSize()
	{
		throw new UnsupportedOperationException();	
	}

	public int getSize()
	{
		throw new UnsupportedOperationException();
	}

	public boolean getIsPrimary()
	{
		boolean f = false;
		return f;
	}

	public BankAccount getBankAccount()
	{
		BankAccount b = null;
		return b;
	}

	public PropertyList getPropertyList()
	{
		PropertyList p = null;
		return p;
	}

	public void profitCalculation()
	{
		profit = revenue - wages;
	}
}