import java.util.*;

/******************************************************************
 * Purpose:  Simple array list to store event objects
 * Import: 
 * Export: 
 * Author: Antony LeGall
 ****************************************************************/

public class EventList
{
    private ArrayList<Event> eventList;
    private int size = 0;

    public EventList()
    {
        eventList = new ArrayList<Event>();
    }

    public void add(Event event)
    {
        eventList.add(event);
    }

    public void updateSize()
    {
        size++;
    }

    public int getSize()
    {
        return size;
    }

    public Event get(int index)
    {
        return eventList.get(index);
    }
}