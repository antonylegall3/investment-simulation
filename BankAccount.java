import java.util.*;

/******************************************************************
 * Purpose:  Container class to store bank account information
 * Import: 
 * Export: 
 * Author: Antony LeGall
 ****************************************************************/

public class BankAccount
{
	private String owner;
	private double balance;
	
	public BankAccount(String inOwner)
	{
		setOwner(inOwner);
		balance = 0.0;
	}

	//Setters
	public void setOwner(String inOwner)
	{
		owner = inOwner;
	}

	public void setBalance(double inBalance)
	{
		balance = balance + inBalance;
	}

	//Getters
	public String getOwner()
	{
		return owner;
	}

	public double getBalance()
	{
		return balance;
	}

	public double profitCalculation()
	{
		double newBalance;
		newBalance = balance * 1.05;
		return newBalance - balance;
	}
}